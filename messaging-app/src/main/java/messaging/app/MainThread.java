package messaging.app;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

public class MainThread {
	private static final Message mainMessage = new Message();
	private static final Chat mainChat = new Chat();
	public static final ConnectServer server = new ConnectServer();
	public static final Deleter deleter = new Deleter();
	public static final PasswordGenerator pg = new PasswordGenerator();
	public static final BackgroundChange bg = new BackgroundChange();
	public static final Encryptor encrypt = new Encryptor();
	public static final ConvertURL urlconverter = new ConvertURL();

	// JTextField
	static JTextField inputField;

	// JFrame
	static JFrame f;

	// JButton
	static JButton sendButton;
	static JButton closeButton;
	static JButton changeBackgroundButton;
	static JButton deleteButton;

	// label to display text
	static JLabel title;

	// Main Thread
	public static void main(String args[]) {
		JPanel main = new JPanel();

		// --------LOGIN LOOP--------
		Boolean loop = true;
		Boolean innerLoop = true;
		String username = "";
		String password = "";

		while (loop) {
			Object[] options = { "Create an Account", "Login", "Cancel" };
			int n = JOptionPane.showOptionDialog(null, "Login", "Message App Login", JOptionPane.DEFAULT_OPTION,
					JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
			// Create an account option
			if (options[n].equals("Create an Account")) {
				JTextField usernameWindow = new JTextField();
				Object[] message = { "Username:", usernameWindow, };

				// TODO hook this into server check
				int option = JOptionPane.showConfirmDialog(null, message, "Create an Account",
						JOptionPane.OK_CANCEL_OPTION);
				if (option == JOptionPane.OK_OPTION) {
					username = usernameWindow.getText();
					// TODO set password by algorithm
					password = pg.generated(username);
					server.connect("INSERT INTO passwordtable (passworddata) VALUES ('" + username + password + "');");
					loop = false;
					JOptionPane.showMessageDialog(null,
							"Account created. Username is: " + username + ". Password is: " + password);
				}
			} else if (options[n].equals("Login")) {
				innerLoop = true;
				while (innerLoop) {
					JTextField usernameWindow = new JTextField();
					JTextField passwordWindow = new JPasswordField();
					Object[] message = { "Username:", usernameWindow, "Password:", passwordWindow };

					// TODO hook this into server check TODO
					int option = JOptionPane.showConfirmDialog(null, message, "Login", JOptionPane.OK_CANCEL_OPTION);
					if (option == JOptionPane.OK_OPTION) {

						username = usernameWindow.getText();
						password = passwordWindow.getText();
						String userReality = username + password;
						ArrayList<String> auth = server.returnPasswordServerData("passwordtable", "passworddata");
						System.out.println("From password table: " + auth.toString());

						if (auth.contains(userReality)) {
							JOptionPane.showMessageDialog(null, "Login Successful");
							innerLoop = false;
							loop = false;
						} else {
							JOptionPane.showMessageDialog(null, "Login Failed. Please try again.");
						}

					} else {
						innerLoop = false;
						// System.exit(0);
					}
				}
			} else {
				loop = false;
				System.exit(0);
			}
		}
		// --------LOGIN LOOP END--------

		mainMessage.setUser(username);

		// ----UI----

		// create a new frame to store text field and button
		f = new JFrame("Messaging App");
		f.setLayout(new BorderLayout());
		BufferedImage img = BackgroundChange.createAndShowInitialGui();
		if (img != null) {
			f.setContentPane(new JLabel(new ImageIcon(img)));
		}

		// create a label to display text
		// testmessage = new JLabel(mainMessage.showData());
		title = new JLabel("Chat Home Page");

		// keep updating chat window on seperate thread
		Runnable r = new Runnable() {
			public void run() {
				while (true) {
					mainChat.updateData(server.returnServerData("messages", "data", true));
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};

		new Thread(r).start();

		// create a new send button
		sendButton = new JButton("Send");
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// save inputed message for chat
				mainMessage.getData(urlconverter.convertURL(inputField.getText()));

				String en = "";
				// encrypt
				try {
					en = encrypt.process(mainMessage.showData());
				} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
						| InvalidKeySpecException | InvalidAlgorithmParameterException | UnsupportedEncodingException
						| IllegalBlockSizeException | BadPaddingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				String statement = "INSERT INTO messages(username, data) VALUES ('" + mainMessage.returnUser() + "','"
						+ en + "');";
				server.connect(statement);
				mainChat.updateData(server.returnServerData("messages", "data", true));
				inputField.setText("");
			}
		});

		// create a new close button
		closeButton = new JButton("Close");
		closeButton.addActionListener(mainChat);

		// create a new change background button
		changeBackgroundButton = new JButton("Change Background");
		changeBackgroundButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// change background
				System.out.println("Changing Background");
				String background = JOptionPane.showInputDialog(null, "Paste image url into text box.");
				BufferedImage img = BackgroundChange.createAndShowNewGui(background);
				if (img != null) {
					f.setContentPane(new JLabel(new ImageIcon(img)));
				}

				// remake window
				f.setLayout(new FlowLayout());
				f.add(main);
				f.pack();
				f.setSize(960, 800);
				f.setBackground(Color.black);
				SwingUtilities.updateComponentTreeUI(f);

				System.out.println("Background Changed");
			}
		});

		// create a new delete button
		deleteButton = new JButton("Delete Messages");
		deleteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// delete user messages
				System.out.println("Deleting");
				int confirmationOne = JOptionPane.showConfirmDialog(null,
						"Are you sure you want to delete your messages?", "Confirm", JOptionPane.YES_NO_OPTION);
				if (confirmationOne == JOptionPane.YES_OPTION) {
					deleter.deleteData("messages", "username", mainMessage.returnUser());
					JOptionPane.showMessageDialog(null, "Your messages have been deleted.");
				}
			}
		});

		// addActionListener to button
		mainChat.getFrame(f);

		// create a object of JTextField with 16 columns
		inputField = new JTextField(64);
		inputField.addActionListener(new ActionListener() {
			@Override
			// enter is pressed
			public void actionPerformed(ActionEvent e) {
				// save inputed message for chat
				mainMessage.getData(urlconverter.convertURL(inputField.getText()));

				String en = "";
				// encrypt
				try {
					en = encrypt.process(mainMessage.showData());
				} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
						| InvalidKeySpecException | InvalidAlgorithmParameterException | UnsupportedEncodingException
						| IllegalBlockSizeException | BadPaddingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				String statement = "INSERT INTO messages(username, data) VALUES ('" + mainMessage.returnUser() + "','"
						+ en + "');";

				server.connect(statement);
				mainChat.updateData(server.returnServerData("messages", "data", true));
				inputField.setText("");
			}
		});

		// create panels
		JPanel chatPanel = new JPanel();
		JPanel messageSendPanel = new JPanel();
		JPanel namePanel = new JPanel();
		JPanel extraPanel = new JPanel();
		JPanel closePanel = new JPanel();

		// add components to panel
		chatPanel.setLayout(new BoxLayout(chatPanel, BoxLayout.PAGE_AXIS));

		chatPanel.add(mainChat.setWindow());
		messageSendPanel.add(inputField);
		messageSendPanel.add(sendButton);
		extraPanel.add(deleteButton);
		extraPanel.add(changeBackgroundButton);
		namePanel.add(title);
		closePanel.add(closeButton);

		//////////////////// constructing window
		// JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		namePanel.setBackground(Color.gray);
		main.add(namePanel, gbc);
		gbc.gridy++;
		gbc.ipady = 400;
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		main.add(chatPanel, gbc);
		gbc.gridy++;
		gbc.weighty = 1;
		gbc.ipady = 0;
		messageSendPanel.setBackground(Color.gray);
		main.add(messageSendPanel, gbc);
		gbc.gridy++;
		gbc.weighty = .5;
		extraPanel.setMaximumSize(extraPanel.getPreferredSize());
		extraPanel.setBackground(Color.gray);
		main.add(extraPanel, gbc);
		gbc.gridy++;
		gbc.weighty = .5;
		closePanel.setMaximumSize(closePanel.getPreferredSize());
		closePanel.setBackground(Color.gray);
		main.add(closePanel, gbc);
		////////////////////////

		// add panel to frame
		f.setLayout(new FlowLayout());
		f.add(main);
		f.pack();

		// set the size of frame
		f.setSize(960, 800);
		f.setBackground(Color.black);

		f.show();

		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

}