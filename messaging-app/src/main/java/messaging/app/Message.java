package messaging.app;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

//Initial implementation for message object
public class Message {

	ArrayList<String> messageData = new ArrayList<String>();
	String user = "";
	String baseUser = "";

	Date date = new Date();
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	String tempMessage = "";

	// Obtain inputed message for object manipulation
	public void getData(String data) {
		// set time
		date = Date.from(Instant.now());

		messageData.add("(" + formatter.format(date) + ") " + user + ": " + data + "<br/>");
		tempMessage = "(" + formatter.format(date) + ") " + user + ": " + data + "<br/>";

	}

	// return message data
	public String showData() {
		String temp = "<html>";

		for (int i = 0; i < messageData.size(); i++) {
			temp = temp + messageData.get(i);
		}

		temp = temp + "</html>";

		return tempMessage;

	}

	// set user name TODO will need to make color it's own variable
	public void setUser(String u) {

		user = "<span style=\"color: #0000A0\">" + u + "</span>";
		baseUser = u;

	}

	// return user name
	public String returnUser() {

		return baseUser;

	}

}
