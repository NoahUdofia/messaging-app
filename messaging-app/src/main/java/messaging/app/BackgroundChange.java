package messaging.app;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

public class BackgroundChange {

	public static final String imageBackground = "https://upload.wikimedia.org/wikipedia/"
			+ "commons/thumb/b/be/Milky_Way_at_Concordia_Camp%2C_Karakoram_Range%2"
			+ "C_Pakistan.jpg/1280px-Milky_Way_at_Concordia_Camp%2C_Karakoram_Range" + "%2C_Pakistan.jpg";
	
	static BufferedImage saved = null;

	static BufferedImage createAndShowInitialGui() {
		BufferedImage img = null;
		try {
			// just using this as an example image, one available to all
			URL imgUrl = new URL(imageBackground); // online path to starry image
			img = ImageIO.read(imgUrl);
		} catch (IOException e) {
			// continue on
		}
		return img;
	}

	static BufferedImage createAndShowNewGui(String newURL) {
		BufferedImage img = null;
		try {
			// just using this as an example image, one available to all
			URL imgUrl = new URL(newURL);
			img = ImageIO.read(imgUrl);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					"Background Set Failed. Make sure your image URL isn't blocked or is entered correctly.");
			img = saved;
		}
		saved = img;
		return img;
	}

}
