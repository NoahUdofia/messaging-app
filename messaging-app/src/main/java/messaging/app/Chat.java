package messaging.app;

import java.util.ArrayList;
import java.awt.Desktop;
import java.awt.event.*;
import javax.swing.*;
import java.util.List;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

//initial implementation for chat box
public class Chat extends JFrame implements ActionListener {

	JFrame frame = new JFrame("Chat");

	JEditorPane textArea = new JTextPane();

	List<String> messages = new ArrayList<String>();

	// add message data
	public void updateData(String temp) {

		textArea.setText(temp);

	}

	// return current message data
	public String returnData() {

		return textArea.getText();

	}

	// get window
	public void getFrame(JFrame f) {

		frame = f;

	}

	// set chat window
	public JScrollPane setWindow() {

		String msgin = " ";

		textArea.addHyperlinkListener(new HyperlinkListener() {
			@Override
			public void hyperlinkUpdate(HyperlinkEvent hle) {
				if (HyperlinkEvent.EventType.ACTIVATED.equals(hle.getEventType())) {
					System.out.println(hle.getURL());
					Desktop desktop = Desktop.getDesktop();
					try {
						desktop.browse(hle.getURL().toURI());
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});

		textArea.setSize(2400, 3200);
		textArea.setEditable(false);
		textArea.setVisible(true);
		textArea.setContentType("text/html");

		JScrollPane scroll = new JScrollPane(textArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		textArea.setText(msgin);

		return scroll;

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String s = e.getActionCommand();
		if (s.equals("Close")) {
			int confirmationOne = JOptionPane.showConfirmDialog(null, "Are you sure you want to close the application?",
					"Confirm", JOptionPane.YES_NO_OPTION);
			if (confirmationOne == JOptionPane.YES_OPTION) {
				frame.dispose();
				System.exit(0);
			}
		}
	}
}
