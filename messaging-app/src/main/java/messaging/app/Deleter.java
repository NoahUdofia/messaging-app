package messaging.app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Deleter {

	static Connection conn = null;
	static String dbName = "messageapp1";
	static String userName = "postgres";
	static String password = "changeit";
	static String hostname = "database-2.cgobym2iep6y.us-east-2.rds.amazonaws.com";
	static String port = "5432";
	static String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName
			+ "&password=" + password;

	// delete user message data
	public void deleteData(String table, String col, String user) {
		try {
			Statement setupStatement = null;

			// Create connection to RDS DB instance
			conn = DriverManager.getConnection(jdbcUrl);

			// Create a table and write two rows
			setupStatement = conn.createStatement();

			setupStatement.addBatch("DELETE FROM " + table + " WHERE " + col + "='" + user + "';");
			setupStatement.executeBatch();
			setupStatement.close();

		} catch (SQLException ex) {
			// Handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			System.out.println("Closing the connection.");
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}
	}

}