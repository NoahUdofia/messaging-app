package messaging.app;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JOptionPane;

public class ConnectServer {

	static String dbName = "messageapp1";
	static String userName = "postgres";
	static String password = "changeit";
	static String hostname = "database-2.cgobym2iep6y.us-east-2.rds.amazonaws.com";
	static String port = "5432";
	static Connection conn = null;
	static Statement setupStatement = null;
	static Statement readStatement = null;
	static ResultSet resultSetUser = null;
	static ResultSet resultSetData = null;
	static String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName
			+ "&password=" + password;

	public static final Decryptor decrypt = new Decryptor();

	public void connect(String insertRow) {

		try {
			// Create connection to RDS DB instance
			conn = DriverManager.getConnection(jdbcUrl);

			// Create a table and write two rows
			setupStatement = conn.createStatement();

			setupStatement.addBatch(insertRow);
			setupStatement.executeBatch();
			setupStatement.close();

		} catch (SQLException ex) {
			// Handle any errors
			System.out.println("SQLException: " + ex.getMessage() + "\n" + "SQLState: " + ex.getSQLState() + "\n"
					+ "VendorError: " + ex.getErrorCode());
			if (ex.getMessage().contains("The connection attempt failed.")) {
				JOptionPane.showMessageDialog(null, "Internet connection lost. Shutting down.");
				System.exit(0);
			}
		} finally {
			System.out.println("Closing the connection.");
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

	}

	public String returnServerData(String tab, String col, Boolean dec) {
		String data = "";

		try {
			conn = DriverManager.getConnection(jdbcUrl);

			readStatement = conn.createStatement(resultSetData.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			resultSetData = readStatement.executeQuery("SELECT " + col + " FROM " + tab + ";");

			String incoming = "";
			while (resultSetData.next()) {

				if (dec) {
					try {
						incoming = decrypt.process(resultSetData.getString(col));
					} catch (InvalidKeyException | InvalidKeySpecException | NoSuchAlgorithmException
							| NoSuchPaddingException | InvalidAlgorithmParameterException | IllegalBlockSizeException
							| BadPaddingException | UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "Error: Decryption failed, unable to get message data. Either database is down or internet isn't connected. Please try again later. Shutting down");
						e.printStackTrace();
						System.exit(0);
					}
				} else {
					incoming = resultSetData.getString(col);
				}

				data += incoming;
			}

			readStatement.close();
			conn.close();
			System.out.println("data: " + data);

		} catch (SQLException ex) {
			// Handle any errors TODO weird "Next" error only occurs here
			System.out.println("SQLException: " + ex.getMessage() + "\n" + "SQLState: " + ex.getSQLState() + "\n"
					+ "VendorError: " + ex.getErrorCode());
			if (ex.getMessage().contains("The connection attempt failed.")) {
				JOptionPane.showMessageDialog(null, "Internet connection lost. Shutting down.");
				System.exit(0);
			}
			//JOptionPane.showMessageDialog(null, "SQL Error: Unable to get message data."); // commented out because they show up randomly yet don't seem to do anything
		} finally {
			System.out.println("Closing the connection.");
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

		return data;
	}

	public ArrayList<String> returnPasswordServerData(String tab, String col) {
		ArrayList<String> data = new ArrayList<String>();
		int count = 0;

		try {
			conn = DriverManager.getConnection(jdbcUrl);

			readStatement = conn.createStatement(resultSetData.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			resultSetData = readStatement.executeQuery("SELECT " + col + " FROM " + tab + ";");

			while (resultSetData.next()) {
				data.add(resultSetData.getString(col));
				count++;
			}

			readStatement.close();
			conn.close();

		} catch (SQLException ex) {
			// Handle any errors TODO weird "Next" error only occurs here
			System.out.println("SQLException: " + ex.getMessage() + "\n" + "SQLState: " + ex.getSQLState() + "\n"
					+ "VendorError: " + ex.getErrorCode());
			//JOptionPane.showMessageDialog(null, "SQL Error: Unable to get message data."); // commented out because they show up randomly yet don't seem to do anything
		} finally {
			System.out.println("Closing the connection.");
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}

		return data;
	}

}
