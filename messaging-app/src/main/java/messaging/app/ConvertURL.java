package messaging.app;

public class ConvertURL {

	// convert string to URL
	// html url layout: <a href="url">link text</a>
	public String convertURL(String originalMessage) {

		String output = "";
		String urlhalf = "";
		String firstpart = "";
		String baseURL = "";
		String lastpart = "";
		String finalURL = "";

		// loop through message for url
		System.out.println("Into Converter: " + originalMessage);
		for (int i = 0; i < originalMessage.length(); i++) {

			if (originalMessage.charAt(i) == 'h') {

				// see if the actual string is logn enough to be a url (see right issue on jira)
				if (originalMessage.substring(i, originalMessage.length()).length() > 4) {
					// see if "http" is at this place in the string
					if (originalMessage.charAt(i + 1) == 't' && originalMessage.charAt(i + 2) == 't'
							&& originalMessage.charAt(i + 3) == 'p') {

						// split off url and wrap for html
						urlhalf = originalMessage.substring(i, originalMessage.length());
						firstpart = originalMessage.substring(0, i);

						// check for if the url is the last thing in the message
						if (urlhalf.contains(" ")) {
							baseURL = urlhalf.substring(0, urlhalf.indexOf(' '));
							lastpart = urlhalf.substring(urlhalf.indexOf(' '));
							finalURL = "<a href=\"" + baseURL + "\">" + baseURL + "</a>";

							output = firstpart + finalURL + convertURL(lastpart);

							break;
						} else {
							finalURL = "<a href=\"" + urlhalf + "\">" + urlhalf + "</a>";

							output = firstpart + finalURL;
						}

					}
				}

			}

		}

		// check if a url was found
		if (output.isEmpty()) {
			return originalMessage;
		} else {
			return output;
		}

	}

}
