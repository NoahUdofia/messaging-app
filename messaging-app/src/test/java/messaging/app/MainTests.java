package messaging.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.awt.image.BufferedImage;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;

import org.junit.Test;

public class MainTests {

	private static final Message mainMessage = new Message();
	private static final Chat mainChat = new Chat();
	private static final ConnectServer testServer = new ConnectServer();
	private static final Deleter testDeleter = new Deleter();
	private static final PasswordGenerator testPG = new PasswordGenerator();
	private static final BackgroundChange testBG = new BackgroundChange();
	public static final Encryptor testEncrypt = new Encryptor();
	public static final Decryptor testDecrypt = new Decryptor();
	public static final ConvertURL testURLConveter = new ConvertURL();

	static String dbName = "messageapp1";
	static String userName = "postgres";
	static String password = "changeit";
	static String hostname = "database-2.cgobym2iep6y.us-east-2.rds.amazonaws.com";
	static String port = "5432";
	static String jdbcUrl = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbName + "?user=" + userName
			+ "&password=" + password;

	static Connection conn = null;

	/**
	 * testMessage() - test initial message class getData
	 */
	@Test
	public void testMessage() {

		String test = "SampleMessage";
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {
			mainMessage.getData(test);
			date = Date.from(Instant.now());
		} catch (Exception e) {
			fail(e.getMessage());
		}
		String expected = "(" + formatter.format(date) + ") " + ": SampleMessage<br/>";
		assertEquals("Message should return the same.'", expected, mainMessage.showData());
	}

	/**
	 * testChatUpdate() - test chat class update function updateData
	 */
	@Test
	public void testChatUpdate() {

		String m = "SampleMessage";
		String expected = "SampleMessage";
		String actual = "";

		try {
			mainChat.updateData(m);
			actual = mainChat.returnData();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals("Message should return the same.'", expected.toString(), actual);
	}

	/**
	 * testChatWindow() - test deployment of chat object setWindow
	 */
	@Test
	public void testChatWindow() {

		List<String> expected = new ArrayList<String>();

		JScrollPane actualScroll = null;

		String m = "SampleMessage";
		expected.add(m);
		expected.add("\n");
		mainMessage.getData(m);

		JScrollPane expectedScroll = new JScrollPane(testTextArea(), JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		try {
			actualScroll = mainChat.setWindow();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals("Message should return the same.'", expectedScroll.toString(), actualScroll.toString());
	}

	/**
	 * testServer() - test if server connection is valid and accepting changes
	 * connect, returnServerData
	 */
	@Test
	public void testServer() {

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Class Error: +" + e1);
		}

		try {
			testServer.connect("INSERT INTO testtable (testdata) VALUES ('Test');");
		} catch (Exception e) {
			fail(e.getMessage());
		}

		String actual = "";
		String expected = "Test";

		actual = testServer.returnServerData("testtable", "testdata", false);

		clearTestDatabase();

		assertEquals("Message should return the same.'", expected, actual);
	}

	/**
	 * testpasswordServerMethod() - test password specific method in server class
	 * connect, returnPasswordServerData
	 */
	@Test
	public void testpasswordServerMethod() {

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Class Error: +" + e1);
		}

		try {
			testServer.connect("INSERT INTO testtable (testdata) VALUES ('TestPassword');");
		} catch (Exception e) {
			fail(e.getMessage());
		}

		ArrayList<String> actual = new ArrayList<String>();
		String expected = "[TestPassword]";

		actual = testServer.returnPasswordServerData("testtable", "testdata");

		clearTestDatabase();

		assertEquals("Message should return the same.'", expected, actual.toString());
	}

	/**
	 * testDeleter() - test delete functionality connect, returnServerData
	 */
	@Test
	public void testDeleter() {

		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("Class Error: +" + e1);
		}

		try {
			testServer.connect("INSERT INTO testtable (testdata) VALUES ('TestDelete');");
		} catch (Exception e) {
			fail(e.getMessage());
		}

		String actual = "";
		String expected = "";

		testDeleter.deleteData("testtable", "testdata", "TestDelete");

		actual = testServer.returnServerData("testtable", "testdata", false);

		// cleanup, doesn't effect results which are saved in actual
		clearTestDatabase();

		assertEquals("Message should return the same.'", expected, actual.toString());
	}

	/**
	 * testPasswordGenerator() - test password generator class generated
	 */
	@Test
	public void testPasswordGenerator() {

		String samplePassword = "SamplePassword";
		String expected = "drowssaPelpmaS";
		String actual = "";

		try {
			actual = testPG.generated(samplePassword);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		assertEquals("Message should return the same.'", expected, actual);
	}

	/**
	 * testURLConverterOne() - Test for url converter convertURL (end on url)
	 */
	@Test
	public void testURLConverterOne() {

		String sampleURL = "this is a sample url https://www.google.com/";
		String expected = "this is a sample url <a href=\"https://www.google.com/\">https://www.google.com/</a>";
		String actual = "";

		actual = testURLConveter.convertURL(sampleURL);

		assertEquals("Message should return the same.'", expected, actual);
	}

	/**
	 * testURLConverterTwo() - Test for url converter convertURL (doesn't end on
	 * URL)
	 */
	@Test
	public void testURLConverterTwo() {

		String sampleURL = "this is a sample url https://www.google.com/ ok?";
		String expected = "this is a sample url <a href=\"https://www.google.com/\">https://www.google.com/</a> ok?";
		String actual = "";

		actual = testURLConveter.convertURL(sampleURL);

		assertEquals("Message should return the same.'", expected, actual);
	}

	/**
	 * testURLConverterThree() - Test for url converter convertURL (No URL)
	 */
	@Test
	public void testURLConverterThree() {

		String sampleURL = "this is a sample url if i had one";
		String expected = "this is a sample url if i had one";
		String actual = "";

		actual = testURLConveter.convertURL(sampleURL);

		assertEquals("Message should return the same.'", expected, actual);
	}

	/**
	 * testSetBackground() - default background set test This test aims to simply
	 * demonstrate that the method returns a buffered image, not its actual contents
	 * createAndShowInitialGui
	 */
	@Test
	public void testSetBackground() {

		String check = "";
		BufferedImage actual = null;

		try {
			actual = testBG.createAndShowInitialGui();
		} catch (Exception e) {
			fail(e.getMessage());
		}
		check = actual.toString();
		assertEquals("BufferedImage should return the same.'", check, actual.toString());
	}

	/**
	 * testEncryptAndDecrypt() - encryption and decryption test process()
	 * 
	 * @throws BadPaddingException, IllegalBlockSizeException,
	 *                              IllegalBlockSizeException,
	 *                              UnsupportedEncodingException,
	 *                              InvalidAlgorithmParameterException,
	 *                              InvalidKeySpecException, NoSuchPaddingException,
	 *                              NoSuchAlgorithmException, InvalidKeyException
	 */
	@Test
	public void testEncryptAndDecrypt() throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeySpecException, InvalidAlgorithmParameterException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {

		String expected = "This is a test message!";
		String actual = "";
		String enc = "";

		enc = testEncrypt.process(expected);

		actual = testDecrypt.process(enc);

		assertEquals("BufferedImage should return the same.'", expected, actual);
	}

	/**
	 * testNewSetBackground() - new background set test This test aims to simply
	 * demonstrate that the method returns a buffered image, not its actual contents
	 * createAndShowNewGui()
	 */
	@Test
	public void testNewSetBackground() {

		String sampleURL = "https://www.ppic.org/wp-content/uploads/water_option2-1440x648.jpg";
		String check = "";
		BufferedImage actual = null;

		try {
			actual = testBG.createAndShowNewGui(sampleURL);
		} catch (Exception e) {
			fail(e.getMessage());
		}
		check = actual.toString();
		assertEquals("BufferedImage should return the same.'", check, actual.toString());
	}

	/**
	 * testNewFailSetBackground() - new background set test This test aims to simply
	 * demonstrate that the method returns a buffered image, not its actual contents
	 * generated
	 */
	/*
	 * @Test public void testNewSetFailBackground() {
	 * 
	 * String sampleURL =
	 * "https://cdnb.artstation.com/p/assets/images/images/037/362/949/large/arden-galdones-fanart-takanashi-kiara-genshin-lowres.jpg?1620181381";
	 * String check = ""; BufferedImage actual = null;
	 * 
	 * try { actual = BackgroundChange.createAndShowNewGui(sampleURL); } catch
	 * (Exception e) { fail(e.getMessage()); } check= actual.toString();
	 * assertEquals("BufferedImage should return the same.'", check,
	 * actual.toString()); }
	 */

	public JTextPane testTextArea() {

		JTextPane textArea = new JTextPane();
		String testMessage = " ";

		textArea.setSize(2400, 3200);
		textArea.setEditable(false);
		textArea.setVisible(true);
		textArea.setText(testMessage);

		return textArea;

	}

	public void clearTestDatabase() {
		try {
			Statement setupStatement = null;

			// Create connection to RDS DB instance
			conn = DriverManager.getConnection(jdbcUrl);

			// Create a table and write two rows
			setupStatement = conn.createStatement();

			setupStatement.addBatch("DELETE FROM testtable WHERE testdata='Test';");
			setupStatement.addBatch("DELETE FROM testtable WHERE testdata='TestPassword';");
			setupStatement.executeBatch();
			setupStatement.close();

		} catch (SQLException ex) {
			// Handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
			System.out.println("Closing the connection.");
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException ignore) {
				}
		}
	}
}
